import numpy as np


def lambda_handler(event, context):
    return {"pi": format(np.pi, '.100g')}


if __name__ == "__main__":
    event = []
    context = []
    print lambda_handler(event, context)

