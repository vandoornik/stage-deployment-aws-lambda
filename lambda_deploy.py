# This file is built upon an early version by joshcb@amazon.com, see the original copyright notice below.
# see also https://bitbucket.org/awslabs/aws-lambda-bitbucket-pipelines-python

# Copyright 2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file
# except in compliance with the License. A copy of the License is located at
#
#     http://aws.amazon.com/apache2.0/
#
# or in the "license" file accompanying this file. This file is distributed on an "AS IS"
# BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under the License.


from __future__ import print_function
import sys
import boto3
from botocore.exceptions import ClientError
import os

lambda_name = os.environ.get('AWS_LAMBDA_NAME')
stage_name = os.environ.get('BITBUCKET_BRANCH', 'dev')
memory_size = os.environ.get('AWS_LAMBDA_MEMORY_SIZE', 512)

DO_NOT_DEPLOY = ['master', ]


def publish_new_version(artifact):
    try:
        client = boto3.client('lambda')
    except ClientError as err:
        print("Failed to create boto3 client.\n" + str(err))
        return False
    try:
        client.update_function_configuration(
            FunctionName=lambda_name,
            MemorySize=int(memory_size)
        )
    except Exception as err:
        print("Failed to read variables.env\n" + str(err))
        return False
    try:
        response = client.update_function_code(
            FunctionName=lambda_name,
            ZipFile=open(artifact, 'rb').read(),
            Publish=True
        )
    except ClientError as err:
        print("Failed to update function code.\n" + str(err))
        return False
    except IOError as err:
        print("Failed to access " + artifact + ".\n" + str(err))
        return False
    version = response['Version']

    try:
        alias_list = client.list_aliases(FunctionName=lambda_name)
    except ClientError as err:
        print("Failed to retrieve Alias names.\n" + str(err))
        return False
    alias_names = [a['Name'] for a in alias_list['Aliases']]
    desired_alias = stage_name.upper()
    if desired_alias in alias_names:
        try:
            response2 = client.update_alias(
                FunctionName=lambda_name,
                Name=desired_alias,
                FunctionVersion=version,
            )
        except ClientError as err:
            print("Failed to update alias.\n" + str(err))
            return False
    else:
        try:
            response2 = client.create_alias(
                FunctionName=lambda_name,
                Name=desired_alias,
                FunctionVersion=version,
            )
        except ClientError as err:
            print("Failed to create alias.\n" + str(err))
            return False
    return response2

if __name__ == "__main__":
    if stage_name not in DO_NOT_DEPLOY:
        if not publish_new_version('artifact.zip'):
            sys.exit(1)
